
# Architecure Ref. Card 01
Standalone Spring Boot Application

Link zur Übersicht:<br/>
https://gitlab.com/bbwrl/m346-ref-card-overview

# Framework

| Szenario                                                     | Provisioning | Config Management    | Build      | Deployment |
| ------------------------------------------------------------ | ------------ | -------------------- | ---------- | ---------- |
| [Inbetriebnahme auf eigenem Computer](#inbetriebnahme-auf-eigenem-computer) | -            | Bash                 | Bash       | Bash       |
| Inbetriebnahme mit multipass                                 | multipass    | Bash                 | Bash       | Bash       |
| Inbetriebnahme mit cloud-init                                | multipass    | cloud-init           | cloud-init | cloud-init |
| [Inbetriebnahme mit Ansible](#inbetriebnahme-mit-ansible)    | multipass    | cloud-init / Ansible | Gitlab-CI  | systemd    |
| [Inbetriebnahme mit Docker](#inbetriebnahme-mit-docker)      | multipass    | cloud-init           | Gitlab-CI  | docker     |
| Inbetriebnahme mit Terraform                                 | terraform    | cloud-init           | Gitlab-CI  | docker     |
| Inbetriebnahme Serverless                                    | terraform    | -                    | Gitlab-CI  | Fargate    |



## Inbetriebnahme auf eigenem Computer

Projekt herunterladen<br/>
```git clone git@gitlab.com:bbwrl/m346-ref-card-01.git```
<br/>
```cd architecture-refcard-01```

### Projekt bauen und starten
Die Ausführung der Befehle erfolgt im Projektordner

Builden mit Maven<br/>
```$ mvn package```

Das Projekt wird gebaut und die entsprechende Jar-Datei im Ordner Target erstellt (Artefakt).

Die erstellte Datei kann nun direkt mit Java gestartet werden.<br/>
```$ java -jar target/architecture-refcard-01-0.0.1-SNAPSHOT.jar```

Die App kann nun im Browser unter der URL http://localhost:8080 betrachtet werden.



## Inbetriebnahme mit Ansible

Mittels `multipass` und `cloud-init` erstellen wir eine virtuelle Maschine. `cloud-init` beschränkt sich in diesem Szenario um die Basisfunktionalität wie Benutzer und Applikationen. Ansible übernimt die komplexeren Konfigurationen. Der systemd-Service startet die Spring Boot Applikation, welche unabhängig zur Installation von Gitlab CI als Artifakt zur Verfügung gestellt wird. 

### Anleitung

Herunterladen der `cloud-init` Konfigurationsdatei [cloud-init-ansible.yml](cloud-init-ansible.yml) mittels `curl` oder `wget`

```sh
$ curl -O https://gitlab.com/bbwin/m346-ref-card-01/-/raw/main/cloud-init-docker.yml
```

Neue VM-Instanz mit cloud-init in Betrieb nehmen

```sh
$ multipass launch -n ref01 --cloud-init cloud-init-ansible.yml
```

Identifizieren der IP-Adresse

```sh
$ multipass list
Name                    State             IPv4             Release
calm-squirrel           RUNNING           10.218.69.109    Ubuntu 20.04 LTS
```

Die Applikation kann im Browser aufgerufen werden: `http://<IPv4>:8080`

### Hintergrund

Normalerweise wird Ansible jeweils von einem Kontroll-Host im Push-Verfahren eingesetzt. Im Cloud-Umfeld ist dies nicht optimal, weshalb mit cloud-init die `ansible-pull` Variante verwendet wird. Anbei die ausgeführte Konfiguration aus der Datei [ansible.yml](ansible.yml) während des cloud-init:

![](ansible-pull.gif)



## Inbetriebnahme mit Docker

Mittels `multipass` und `cloud-init` erstellen wir eine docker-enabled virtuelle Maschine. Der Build-Prozess läuft automatisiert über Gitlab anhand Instruktionen in der Datei [Dockerfile](Dockerfile) und [.gilab-ci.yml](.gilab-ci.yml). Dies geschieht unabhängig von der Inbetriebnahme der virtuellen Maschine jeweils beim Einchecken von neuem Code in dieses Repository. Der Docker-Container wird mittels `cloud-init` gestartet.

### Anleitung

Herunterladen der `cloud-init` Konfigurationsdatei [cloud-init-docker.yml](cloud-init-docker.yml) mittels `curl` oder `wget`

```sh
$ curl -O https://gitlab.com/bbwin/m346-ref-card-01/-/raw/main/cloud-init-docker.yml
```

Neue VM-Instanz mit cloud-init in Betrieb nehmen

```sh
$ multipass launch -n ref01 --cloud-init cloud-init-docker.yml
```

Identifizieren der IP-Adresse

```sh
$ multipass list
Name                    State             IPv4             Release
calm-squirrel           RUNNING           10.218.69.109    Ubuntu 20.04 LTS
```

Die Applikation kann im Browser aufgerufen werden: `http://<IPv4>:8080`

## Landscape

### Provisioning

- multipass
- Vagrant
- Terraform

### Configuration Management

- Scripts / Bash
- cloud-init
- Ansible

### Build

- Scripts / Bash
- cloud-init
- Docker / Docker-Compose
- Gitlab CI/CD

### Deployment

- Scripts / Bash
- Docker
