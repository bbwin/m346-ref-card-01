FROM maven:3.8-openjdk-11-slim as builder

COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package

FROM adoptopenjdk/openjdk11:jre-11.0.11_9-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=/target/*.jar
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
