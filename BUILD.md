# Build Spring Boot Application

Ein Applikationsentwickler betreibt während der Entwicklung einer App jeweils seine eigene, lokale Entwicklungsumgebung. Als Plattformentwickler sind wir jedoch viel mehr am Betrieb als an den Abhängigkeiten interessiert. Anbei die Installationsanleitung für den m346-ref-card-01 Stack

## Installationsanleitung lokale Umgebung
Installieren von Maven

```sh
sudo apt update
sudo apt install -y maven
```

Installieren Java 17

```sh
sudo apt install -y openjdk-17-jdk openjdk-17-jre
```

Installieren von git

```SH
sudo apt install -y git
```

Clonen des Ref Card 01

```sh
mkdir -p workspace
cd workspace
git clone https://gitlab.com/bbwrl/m347-ref-card-01
cd m347-ref-card-01
```

Spring Boot kompilieren

```sh
maven package
```

Als Ergebnis haben Sie anschliessend ein Verzeichnis `target` mit den finalen Jar-Dateien welche Sie mit `java -jar app.jar` ausführen können.

## Dockerfile
Mittels Docker können wir dies alles in Docker auslagern. Lassen Sie uns die Befehle von oben der Reihe nach in ein Dockerfile schreiben:
### Maven
Anstatt maven mittels `sudo apt install -y maven` lokal zu installieren, können wir im Dockerfile ein fixfertiges Maven benutzen.
```sh
FROM maven:3.9.0-eclipse-temurin-17-alpine

COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package
```

Dieser Teil lädt mittels `FROM` ein Maven mit OpenJDK und kopiert anschliessend lokal das Verzeichnis `/src` sowie die Datei `pom.xml` in einen Container. 

### OpenJRE
Ähnliches geschieht mit der daraus resultierenden Jar-Datei. Anstatt die Java Runtime mit `sudo apt install -y openjdk-17-jre" zu installieren, können wir dies im Dockerfile direkt ausführen.
```sh
FROM eclipse-temurin:17-jdk-alpine
ARG JAR_FILE=/target/*.jar
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

## Schritt-für-Schritt Anleitung

### Clonen des Ref Card 01

```sh
mkdir -p workspace
cd workspace
git clone https://gitlab.com/bbwrl/m347-ref-card-01
cd m347-ref-card-01
```

### Dockerfile erstellen
Mit VS Code oder Nano/Vim die Datei `Dockerfile` erstellen (`nano Dockerfile`)
```sh
FROM maven:3.9.0-eclipse-temurin-17-alpine as builder

COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package

FROM eclipse-temurin:17-jdk-alpine
ARG JAR_FILE=/target/*.jar
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

### Docker build
Erstellen Sie das Image mit Docker und setzen Sie einen Tag
```sh
docker build --tag=ref-card-01:latest .
```

### Docker image
Überprüfen Sie mit `docker images` ob das Image korrekt erstellt und gespeichert wurde
```sh
$ docker images
REPOSITORY                                      TAG       IMAGE ID       CREATED          SIZE
ref-card-01                                     latest    9f4446f8175d   23 seconds ago   377MB
```

### Docker run
Versuchen Sie nun den Container zu starten
```sh
docker run -p 8080:8080 ref-card-01:latest
```

Sie sehen die Applikation im Erfolgsfall unter [localhost:8080 - Application Ref. Card 01](http://localhost:8080/)

## Nächste Schritte

Das Image `eclipse-temurin:17-jdk-alpine` beinhaltet immer noch eine JDK - wieso ist dies nicht wünschenswert?

Versuchen Sie das `Dockerfile` weiter zu optimieren. Versuchen Sie ein möglichst kleines Image zu erzeugen.

Als Hilfestellung nehmen Sie [9 Tips for Containerizing Your Spring Boot Code | Docker](https://www.docker.com/blog/9-tips-for-containerizing-your-spring-boot-code/) und [eclipse-temurin - Official Image | Docker Hub](https://hub.docker.com/_/eclipse-temurin).