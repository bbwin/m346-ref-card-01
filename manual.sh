#!/bin/bash
sudo apt upgrade
sudo apt install openjdk-11-jdk
sudo apt install maven
java -version
git clone https://gitlab.com/bbwrl/m346-ref-card-01.git
cd m346-ref-card-01
maven package
java -jar m346-ref-card-01/target/architecture-refcard-01-0.0.1-SNAPSHOT.jar &